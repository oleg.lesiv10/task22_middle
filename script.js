function generateTable()
{
    checkForTable();
    var n = document.getElementById("nInput").value;
    var m = document.getElementById("mInput").value;
    var table = document.createElement("table");
    for(let i = 0 ; i < n; i++)
    {
        let tr = document.createElement("tr");

        for(let j = 0; j < m; j++)
        {
            let td = document.createElement("td");
            td.style.background = "white";
            let text = document.createTextNode((i+1).toString() + (j+1).toString());
            td.addEventListener("click", changeColor);
            td.appendChild(text);
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    document.body.appendChild(table);
}
function checkForTable()
{
    let table = document.getElementsByTagName("table")[0];
    if(table != null) table.remove(); 
}
function changeColor()
{
    if(this.style.background !== "white")
    {
        this.style.background = "white";
        return false;
    }
    this.style.background = getRandomColor();
}
function getRandomColor()
{
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}